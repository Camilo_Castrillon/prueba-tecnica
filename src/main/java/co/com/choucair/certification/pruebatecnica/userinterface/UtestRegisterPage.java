package co.com.choucair.certification.pruebatecnica.userinterface;

import net.serenitybdd.screenplay.actions.selectactions.SelectByValueFromTarget;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class UtestRegisterPage {
    public static final Target REGISTER_BUTTON = Target.the("button that shows us the form to register")
            .located(By.xpath("//a[contains(text(), 'Join Today')]"));

    public static final Target FIRST_NAME = Target.the("where do we write the first Name")
            .located(By.id("firstName"));

    public static final Target LAST_NAME = Target.the("where do we write the last Name")
            .located(By.id("lastName"));

    public static final Target EMAIL = Target.the("where do we write the email")
            .located(By.id("email"));

    public static final Target MONTH = Target.the("where do we write the month")
            .located(By.id("birthMonth"));

    public static final Target DAY = Target.the("where do we write the day")
            .located(By.id("birthDay"));

    public static final Target YEAR = Target.the("where do we write the year")
            .located(By.id("birthYear"));

    public static final Target LANGUAGES = Target.the("where do we write the year")
            .located(By.xpath("//*[@id=\"languages\"]/div[1]/input"));

    public static final Target NEXT_BUTTON = Target.the("Button to confirm for continue to next form")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/a/span"));

    public static final Target NEXT2_BUTTON = Target.the("Button to confirm for continue to next form")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a/span"));

    public static final Target SELECT_MOBILE = Target.the("Select type device mobile")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[1]/div[2]/div/div[1]/span"));

    public static final Target SELECT_MOBILE2 = Target.the("Select type device mobile")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[1]/div[2]/div/input[1]"));

    public static final Target MODEL = Target.the("Select type device mobile")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]/div/div[1]/span"));

    public static final Target MODEL2 = Target.the("Select type device mobile")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]/div/input[1]"));

    public static final Target OPERATING_SYSTEM = Target.the("Select type device mobile")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div/div[1]/span"));

    public static final Target OPERATING_SYSTEM2 = Target.the("Select type device mobile")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div/input[1]"));

    public static final Target LAST_STEP = Target.the("Button to confirm for continue to last form")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/div[2]/div/a/span"));


    public static final Target PASSWORD = Target.the("where do we write the password")
            .located(By.id("password"));

    public static final Target CONFIRM_PASSWORD = Target.the("where do we write the confirm password")
            .located(By.id("confirmPassword"));

    public static final Target TERMS = Target.the("Aceept terms of use")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));

    public static final Target PRIVACY = Target.the("Aceept terms of use")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));

    public static final Target FINISH_BUTTON = Target.the("Button to finish register")
            .located(By.xpath("//a[contains(@class, 'btn btn-blue')]"));
}