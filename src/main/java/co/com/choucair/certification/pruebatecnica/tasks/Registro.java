package co.com.choucair.certification.pruebatecnica.tasks;

import co.com.choucair.certification.pruebatecnica.userinterface.UtestRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.serenitybdd.screenplay.waits.Wait;
import net.serenitybdd.screenplay.waits.WaitUntilBuilder;
import net.thucydides.core.steps.WaitForBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

public class Registro implements Task {

    private String strFirstName;
    private String strLastName;
    private String strEmailAddress;
    private String strMonth;
    private String strDay;
    private String strYear;
    private String strLanguage;
    private String strSelectMobile;
    private String strModel;
    private String strOperatingSystem;
    private String strPassword;
    private String strConfirmPassword;

    public Registro(String strFirstName, String strLastName, String strEmailAddress, String strMonth,
                    String strDay, String strYear, String strLanguage, String strSelectMobile,String strModel,
                    String strOperatingSystem, String strPassword, String strConfirmPassword) {
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmailAddress = strEmailAddress;
        this.strMonth = strMonth;
        this.strDay = strDay;
        this.strYear = strYear;
        this.strLanguage = strLanguage;
        this.strSelectMobile = strSelectMobile;
        this.strModel = strModel;
        this.strOperatingSystem = strOperatingSystem;
        this.strPassword = strPassword;
        this.strConfirmPassword = strConfirmPassword;

    }

    public static Registro OnThePage(String strFirstName, String strLastName,String strEmailAdrress,
                                     String strMonth, String strDay, String strYear, String strLanguage,
                                     String strSelectMobile, String strModel, String strOperatingSystem,
                                     String strPassword, String strConfirmPassword) {
        return Tasks.instrumented(Registro.class,strFirstName,strLastName,strEmailAdrress,
                strMonth,strDay,strYear,strLanguage, strSelectMobile, strModel, strOperatingSystem,
                strPassword, strConfirmPassword);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(UtestRegisterPage.REGISTER_BUTTON),
                Enter.theValue(strFirstName).into(UtestRegisterPage.FIRST_NAME),
                Enter.theValue(strLastName).into(UtestRegisterPage.LAST_NAME),
                Enter.theValue(strEmailAddress).into(UtestRegisterPage.EMAIL),
                SelectFromOptions.byVisibleText(strMonth).from(UtestRegisterPage.MONTH),
                SelectFromOptions.byVisibleText(strDay).from(UtestRegisterPage.DAY),
                SelectFromOptions.byVisibleText(strYear).from(UtestRegisterPage.YEAR),
                Enter.theValue(strLanguage).into(UtestRegisterPage.LANGUAGES).thenHit(Keys.ENTER),
                Click.on(UtestRegisterPage.NEXT_BUTTON)

        );
        actor.attemptsTo(Wait.until(
                WebElementQuestion.the(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/input[1]")) , WebElementStateMatchers.isEnabled()
        ).forNoLongerThan(10).seconds());

        actor.attemptsTo(
                Click.on(UtestRegisterPage.NEXT2_BUTTON),
                Click.on(UtestRegisterPage.SELECT_MOBILE),
                Enter.theValue(strSelectMobile).into(UtestRegisterPage.SELECT_MOBILE2).thenHit(Keys.ENTER),
                Click.on(UtestRegisterPage.MODEL),
                Enter.theValue(strModel).into(UtestRegisterPage.MODEL2).thenHit(Keys.ENTER),
                Click.on(UtestRegisterPage.OPERATING_SYSTEM),
                Enter.theValue(strOperatingSystem).into(UtestRegisterPage.OPERATING_SYSTEM2).thenHit(Keys.ENTER),
                Click.on(UtestRegisterPage.LAST_STEP),
                Enter.theValue(strPassword).into(UtestRegisterPage.PASSWORD),
                Enter.theValue(strConfirmPassword).into(UtestRegisterPage.CONFIRM_PASSWORD),
                Click.on(UtestRegisterPage.TERMS),
                Click.on(UtestRegisterPage.PRIVACY),
                Click.on(UtestRegisterPage.FINISH_BUTTON)
        );

    }
}