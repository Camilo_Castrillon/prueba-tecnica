package co.com.choucair.certification.pruebatecnica.Model;

public class UTestData {

    private String strFirstName;
    private String strLastName;
    private String strEmailAddress;
    private String strMonth;
    private String strDay;
    private String strYear;
    private String strLanguaje;
    private String strSelectMobile;
    private String strModel;
    private String strOperatingSystem;
    private String strPassword;
    private String strConfirmPassword;

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getStrEmailAddress() {
        return strEmailAddress;
    }

    public void setStrEmailAddress(String strEmailAddress) {
        this.strEmailAddress = strEmailAddress;
    }

    public String getStrMonth() {
        return strMonth;
    }

    public void setStrMonth(String strMonth) {
        this.strMonth = strMonth;
    }

    public String getStrDay() {
        return strDay;
    }

    public void setStrDay(String strDay) {
        this.strDay = strDay;
    }

    public String getStrYear() {
        return strYear;
    }

    public void setStrYear(String strYear) {
        this.strYear = strYear;
    }

    public String getStrLanguaje() {
        return strLanguaje;
    }

    public void setStrLanguaje(String strLanguaje) {
        this.strLanguaje = strLanguaje;
    }

    public String getStrSelectMobile() {
        return strSelectMobile;
    }

    public void setStrSelectMobile(String strSelectMobile) {
        this.strSelectMobile = strSelectMobile;
    }

    public String getStrModel() {
        return strModel;
    }

    public void setStrModel(String strModel) {
        this.strModel = strModel;
    }

    public String getStrOperatingSystem() {
        return strOperatingSystem;
    }

    public void setStrOperatingSystem(String strOperatingSystem) {
        this.strOperatingSystem = strOperatingSystem;
    }

    public String getStrPassword() {
        return strPassword;
    }

    public void setStrPassword(String strPassword) {
        this.strPassword = strPassword;
    }


    public String getStrConfirmPassword() {
        return strConfirmPassword;
    }

    public void setStrConfirmPassword(String strConfirmPassword) {
        this.strConfirmPassword = strConfirmPassword;
    }
}
