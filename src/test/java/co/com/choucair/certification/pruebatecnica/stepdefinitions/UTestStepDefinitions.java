package co.com.choucair.certification.pruebatecnica.stepdefinitions;

import co.com.choucair.certification.pruebatecnica.Model.UTestData;
import co.com.choucair.certification.pruebatecnica.tasks.OpenPage;
import co.com.choucair.certification.pruebatecnica.tasks.Registro;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import java.util.List;

public class UTestStepDefinitions {
    @Before
    public void setStage (){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than camilo wants to learn automation at the utest$")
    public void thanCamiloWantsToLearnAutomationAtTheUtest(List<UTestData> uTestData) throws Exception{
        OnStage.theActorCalled("Camilo").wasAbleTo(OpenPage.ThePage(), Registro
                .OnThePage(uTestData.get(0).getStrFirstName(),uTestData.get(0).getStrLastName(),
                        uTestData.get(0).getStrEmailAddress(), uTestData.get(0).getStrMonth(),
                        uTestData.get(0).getStrDay(),uTestData.get(0).getStrYear(),
                        uTestData.get(0).getStrLanguaje(), uTestData.get(0).getStrSelectMobile(),
                        uTestData.get(0).getStrModel(), uTestData.get(0).getStrOperatingSystem(),
                        uTestData.get(0).getStrPassword(),uTestData.get(0).getStrConfirmPassword()));
    }

    @When("^he register a user on the utest platform$")
    public void heRegisterAUserOnTheUtestPlatform() {
    }

    @Then("^Register correctly$")
    public void registerCorrectly() {
    }

}
